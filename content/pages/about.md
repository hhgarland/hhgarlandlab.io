---
title: About
description: "My personal knowledgebase for telecommunications and VoIP topics"
date: 2023-05-28T09:31:10-05:00
---

Hello and welcome to my blog where I post about all things VoIP. This is somewhat of a knowledge base for my myself, but I hope others find this site useful. I can't take much credit for the information here as it's been cobbled together from other sites, blogs, discussion posts, etc..

A few other blogs I follow:

[NOC Thoughts](https://nocthoughts.com/)  
[Voice in Peace VIP](https://melvinleejr.blogspot.com/)  
[Cisco Unified Communications and Collaboration](https://ciscouccollab.blogspot.com/)

